<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/17/2018
 * Time: 3:41 PM
 */

namespace App\Http\Controllers;


use App\Models\BlogsModels;
use App\Models\CallbackModel;
use App\Models\PagesBlockModel;
use App\Models\PagesModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller
{
    public function index()
    {
        $sitemap = App::make('sitemap');
        $sitemap->setCache('laravel.sitemap', 60);
        if (!$sitemap->isCached()) {
            $translations = [
                ['language' => '/'],
                ['language' => '/en/'],
                ['language' => '/it/'],
            ];

            foreach ($translations as $translation) {
                $sitemap->add(URL::to($translation['language']), '2019-01-23:10:00+02:00', '1.0', 'daily');
                $sitemap->add(URL::to($translation['language'] . 'contacts'), '2019-01-23T12:30:00+02:00', '0.9', 'monthly');
                $sitemap->add(URL::to($translation['language'] . 'about'), '2019-01-23T12:30:00+02:00', '1.0', 'daily');
                $sitemap->add(URL::to($translation['language'] . 'articles'), '2019-01-23T12:30:00+02:00', '1.0', 'daily');
                $sitemap->add(URL::to($translation['language'] . 'privacy-policy'), '2019-01-23T12:30:00+02:00', '1.0', 'daily');

            }

            $model = \App\Models\PagesModel::where('type', 0)->orderBy('position', 'ASC')->get();
            foreach ($model as $item) {
                $sitemap->add(URL::to('/' . $item->locale . '/' . $item->alias), $item->updated_at, '1.0', 'daily');
            }

            $modelBlog = \App\Models\BlogsModels::where('status', 1)->get();
            foreach ($modelBlog as $blog) {
                $images = [];
                $images[] = ['url' => URL::to("/storage/" . $blog->image), 'title' => $blog->title];
                $sitemap->add(URL::to('/' . $blog->locale . '/' . $blog->alias), $blog->updated_at, '1.0', 'daily', $images);
            }

        }
        $sitemap->store('xml', 'sitemap');

        return view('welcome');
    }

    public function articles($alias = null)
    {
        $blogs = \App\Models\BlogsModels::where('status', 1)->where('locale', Lang::getLocale())->get();
        if (!empty($alias)) {
            $blogs = \App\Models\BlogsModels::where('status', 1)->where('category', $alias)->where('locale', Lang::getLocale())->get();
        }
        return view('articles', ['blogs' => $blogs]);
    }

    public function search(Request $request)
    {
        $search = $request->input('search');
        $blogs = \App\Models\BlogsModels::where('status', 1)->where('locale', Lang::getLocale())
            ->whereRaw(' title LIKE "%' . $search . '%" OR ' . 'body LIKE "%' . $search . '%" ')->get();
        return view('articles', ['blogs' => $blogs]);
    }

    public function article($alias)
    {
        $model = BlogsModels::findByAlias($alias, Lang::getLocale());
        if (empty($model)) {
            $modelOther = BlogsModels::findByAlias($alias, Lang::getLocale(), true);
            if (!empty($modelOther)) {
                return view('article', ['blog' => $model, 'error' => true]);
            } else {
                abort(404);
            }
        } else {
            return view('article', ['blog' => $model]);
        }
    }

    public function contacts()
    {
        $model = PagesBlockModel::getByLocal(PagesBlockModel::TYPE_CONTACTS, Lang::getLocale());
        return view('contacts', ['model' => $model]);
    }

    public function about()
    {
        $blocks = PagesBlockModel::getByLocalAll(PagesBlockModel::TYPE_ABOUT, Lang::getLocale());
        return view('about', ['models' => $blocks]);
    }

    public function pages($alias)
    {

        switch ($alias) {
            case "logistic":
            case "quality-check":
            case "representative":
            case "suppliers":
            case "analysis":
            case "translator":
                $model = PagesModel::findByAlias($alias, Lang::getLocale());
                return view('page', ['alias' => $alias, 'model' => $model]);
            case 'privacy-policy':
                $model = PagesModel::findByAlias($alias, Lang::getLocale());
                return view('page', ['alias' => $alias, 'model' => $model]);
            default:
                abort(404);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function callback(Request $request)
    {
        $name = $request->input('name',  '');
        $service = $request->input('service', 0);
        $email = $request->input('email', '');
        $message = $request->input('msg', '');

        $model = new CallbackModel();

        $model->name = $name;
        $model->service = $service;
        $model->email = $email;
        $model->msg = $message;
        $model->status = 0;

        $model->save();

        session(['showFlash' => 'true']);

        switch (Lang::getLocale()) {
            case "ru":
                $email = "sales.ru@relycom.net";
                break;
            case "en":
                $email = "sales.eng@relycom.net";
                break;
            case "it":
                $email = "sales.it@relycom.net";
                break;
            default:
                $email = "sales.ru@relycom.net";
        }

       Mail::send('emails.callback', ['callback' => $model], function ($m) use ($email, $model) {
            $m->from('Sales.eng@relycom.net', 'Relycom.net');
            $m->to($email, 'Relycom.net')
                ->subject("New query from site");
        });

        return redirect('contacts');
    }

    public static function getLocal()
    {
        $domain = array_first(explode('.', \Request::getHost()));
        if (!empty($domain))
            if ($domain == 'it' || $domain == 'en' || $domain == 'ru')
                return $domain;
        return "en";
    }

}