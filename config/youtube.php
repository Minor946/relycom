<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Youtube
    |--------------------------------------------------------------------------
    |
    */

    'rel' => 0,

    'autoplay' => 1,

    'controls' => 0,

    'loop' => 0,

    'showinfo' => 0,

    'width' => '100%',

    'height' => 360,

    'frameborder' => 0,

    'bootstrap-responsive-embed' => false,

    'bootstrap-ratio' => '16by9',

];