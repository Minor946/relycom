<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/17/2018
 * Time: 5:09 PM
 */
?>
@extends('layouts.layout')

@section('meta')
    @parent
    <meta name="title" content="{{ $model->title }}">
    <?php if(!empty($model->meta_description)):?>
    <meta name="description" content="{{ $model->meta_description }}">
    <?php endif;?>
    <meta name="keywords" content="{{ $model->meta_tag }}">
@endsection

@if(!empty($model))
    @section('title', $model->title)
@endif

@section('content')
    @if($alias == "logistic")
        @include('components.services.logistic',['model'=>$model])
    @elseif($alias == "quality-check")
        @include('components.services.quality',['model'=>$model])
    @elseif($alias == "representative")
        @include('components.services.representative',['model'=>$model])
    @elseif($alias == "suppliers")
        @include('components.services.suppliers',['model'=>$model])
    @elseif($alias == "analysis")
        @include('components.services.analysis',['model'=>$model])
    @elseif($alias == "translator")
        @include('components.services.translator',['model'=>$model])
    @elseif(!empty($model))
        @include('components.pages.view',['model'=>$model])
    @endif
@endsection

