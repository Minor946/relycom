<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 06.02.2019
 * Time: 1:58
 */
/** @var \App\Models\CallbackModel $callback */
?>

<p><strong>Name:</strong><?= $callback->name ?></p>
<p><strong>Service:</strong><?= $callback->service ?></p>
<p><strong>Email:</strong><?= $callback->email ?></p>
<p><strong>Message:</strong><?= $callback->msg ?></p>
<p><strong>Send date:</strong><?= $callback->created_at ?></p>