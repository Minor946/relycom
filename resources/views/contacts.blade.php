<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/17/2018
 * Time: 5:08 PM
 */
?>

@extends('layouts.layout')

@section('title', __('title.Contacts'))

@section('meta')
    @parent
    <meta name="description" content="{{__('meta.ContactsDescription')}}">
    <meta name="keywords" content="{{__('meta.ContactsKeywords')}}">
@endsection

@section('content')
    @include('components.contacts.view', ['model'=> $model])
@endsection

@section('footer')
    @include('layouts.footer_small')
@endsection
