<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/17/2018
 * Time: 5:09 PM
 */
?>
@extends('layouts.layout')

@section('title', __('title.About'))

@section('meta')
    @parent
    <meta name="description" content="{{__('meta.AboutDescription')}}">
    <meta name="keywords" content="{{__('meta.AboutKeywords')}}">
@endsection

@section('content')
    @include('components.about.view', ['models'=>$models])
@endsection

