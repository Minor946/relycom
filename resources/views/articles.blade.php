<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/17/2018
 * Time: 5:08 PM
 */
?>
@extends('layouts.layout')

@section('title', __('title.Articles'))

@section('content')
    @include("components.blogs.list")
@endsection

