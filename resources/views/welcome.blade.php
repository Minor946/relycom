<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/17/2018
 * Time: 3:56 PM
 */
?>

@extends('layouts.layout')

@section('title', __('title.MainPage'))

@section('meta')
    @parent
    <meta name="description" content="{{__('meta.MainPageDescription')}}">
    <meta name="keywords" content="{{__('meta.MainPageKeywords')}}">
@endsection


@section('content')
    <?php $firstLoad = session('firstLoad', 'true'); ?>
    @if($firstLoad)
        @desktop
        <div id="loader-wrapper">
            <div id="loader">
                <div id="loader-brand"></div>
            </div>
        </div>
        {{Session::put('firstLoad', false)}}

        <style>

            .wrapper {
                margin-top: 0 !important;
            }
        </style>
        @enddesktop
    @endif


    @desktop
    @include('components.main.logo')
    @enddesktop
    @include('components.main.view')
    @include('components.main.partners')
@endsection

@section('scripts')
    @parent
    $(document).ready(function ($) {
    animatePage('loader-brand','{{url('/animate/load.json')}}', false);
    });
@endsection