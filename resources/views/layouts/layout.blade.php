<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/17/2018
 * Time: 3:56 PM
 */
?>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-134674550-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-134674550-1');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Relycom - @yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="icon" type="image/png" href="{{{ url('/favicon.ico') }}}">
    @yield('meta')

    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" type="text/css">

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(52422931, "init", {
            id:52422931,
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/52422931" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '293037234655191');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1"
             src="https://www.facebook.com/tr?id=293037234655191&ev=PageView
&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

</head>

<body>
<div id="app">
    @section('navbar')
        @include('layouts.navbar')
    @show

    <div class="wrapper">
        @yield('content')
    </div>

    @section('footer')
        @include('layouts.footer')
    @show
</div>

<div id="cookieConsent">
    <div id="closeCookieConsent">x</div>
    {{__('This website is using cookies.')}} <a href="{{'/privacy-policy'}}" target="_blank">{{__('More info')}}</a>. <a class="cookieConsentOK">{{__("That's Fine")}}</a>
</div>


<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bodymovin.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/aos.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.visible.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/js.cookie.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/scripts.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    @yield('scripts')
</script>


</body>
</html>