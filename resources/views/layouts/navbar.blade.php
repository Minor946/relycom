<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/17/2018
 * Time: 3:56 PM
 */

use Illuminate\Support\Facades\Lang;

$count = 1;
?>

<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-grey">
    <div class="container">
        <div class="row">
            <div class="col-2 col-md-4 col-lg-2">
                <a id="first-logo" class="navbar-brand" href="{{url("/")}}">
                    <img src="{{url('/images/logo_menu.svg')}}" width="150">
                </a>
            </div>
            <div class="col-10 col-md-8 col-lg-10">
                @handheld
                <button class="hamburger hamburger--squeeze" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
                </button>
                @endhandheld
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item <?= (str_replace(\Illuminate\Support\Facades\Lang::getLocale().'/','', Request::path()) == '/') ? 'active' : '' ?>">
                            <a class="nav-link" href="{{url("/")}}">{{__('navbar.Main')}}</a>
                        </li>
                        <li class="nav-item {{ (str_replace(\Illuminate\Support\Facades\Lang::getLocale().'/','', Request::path()) == 'about') ? 'active' : '' }}">
                            <a class="nav-link" href="{{url('/about')}}">{{__('navbar.About')}}</a>
                        </li>
                        <li class="nav-item dropdown {{ in_array((str_replace(\Illuminate\Support\Facades\Lang::getLocale().'/','', Request::path())), ['quality-check','logistic','representative','suppliers','analysis','translator']) ? 'active' : '' }}">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{__('navbar.Service')}}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <?php
                                $model =  \App\Models\PagesModel::where('type', 0)->where('locale', Lang::getLocale())->orderBy('position','ASC')->get();
                                ?>
                                <?php foreach ($model as $item):?>
                                    <a class="dropdown-item <?= (str_replace(\Illuminate\Support\Facades\Lang::getLocale().'/','', Request::path()) == $item->alias) ? 'active' : ''?>"
                                       href="{{url('/'.$item->alias)}}">{{$item->title_menu}}</a>
                                <?php endforeach;?>
                            </div>
                        </li>
                        <li class="nav-item {{ (str_replace(\Illuminate\Support\Facades\Lang::getLocale().'/','', Request::path()) == 'articles') ? 'active' : '' }}">
                            <a class="nav-link" href="{{url('/articles')}}">{{__('navbar.Articles')}}</a>
                        </li>
                        <li class="nav-item {{ (str_replace(\Illuminate\Support\Facades\Lang::getLocale().'/','', Request::path()) == 'contacts') ? 'active' : '' }}">
                            <a class="nav-link" href="{{url('/contacts')}}">{{__('navbar.Contacts')}}</a>
                        </li>
                    </ul>
                    <div class="form-inline my-2 my-lg-0">
                        <ul class="navbar-nav navbar-lang mr-auto">
                            @foreach(LaravelLocalization::getLocalesOrder() as $localeCode => $properties)
                                <li class="nav-item @if((LaravelLocalization::getCurrentLocale()  == $localeCode) ) active @endif">
                                    <a class="nav-link" rel="alternate" hreflang="{{ $localeCode }}"
                                       href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                        {{ $properties['short'] }}
                                    </a>
                                </li>
                                @if($count < count(LaravelLocalization::getLocalesOrder()))
                                    <li class="nav-item active">
                                        <p class="nav-link" rel="alternate"
                                           href="javascript:void(0)">/</p>
                                    </li>
                                    <?php $count++ ?>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    @handheld
                    <ul class="footer-social">
                        <?php $social = \App\Models\SocialModels::where('status', 1)->get();?>
                        <?php if(!empty($social)):?>
                        <?php foreach ($social as $item):?>
                        <li><a href="{{$item->url}}" target="_blank"><i class="fa fa-2x {{$item->icon}}"></i></a></li>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>

                    <a class="navbar-brand" href="{{url("/")}}"><img src="{{url('/images/logo_menu.svg')}}" width="150"> </a>
                    @endhandheld
            </div>
        </div>
        </div>
    </div>
</nav>