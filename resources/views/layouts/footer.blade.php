<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/17/2018
 * Time: 3:56 PM
 */
?>

<footer @desktop data-aos="fade-up" @enddesktop>
    <div class="container ">
        <div class="row">
            <div class="col-12 col-md-5"></div>
            <div class="col-12 col-md-6">
                <img src="{{url('/images/logo_menu.svg')}}" width="150" style="margin-bottom: 10px">
                <div class="footer-container">
                    <div style="display: flex">
                        <i class="fa fa-map-marker" style="margin: auto 0;">&nbsp;</i>
                        <p style="margin: 0">&nbsp;&nbsp;{!! __("Китай, провинция Гуандун,<br>&nbsp; г. Гуанчжоу, ул Тунхэ 191") !!} </p>
                    </div>
                    <p class="margin-top-5"><i class="fa fa-phone">&nbsp;&nbsp;</i>{{__("+123 456 789 0123")}}</p>
                    <p class="email-link"><a href="{{url('/contacts#contacts-form')}}"><i class="fa fa-envelope">&nbsp;&nbsp;</i>{{__("sales.ru@relycom.net")}}  </a> </p>
                </div>
                <ul class="footer-social">
                    <?php $social = \App\Models\SocialModels::where('status', 1)->get();?>
                    <?php if(!empty($social)):?>
                        <?php foreach ($social as $item):?>
                            <li><a href="{{$item->url}}" target="_blank"><i class="fa fa-2x {{$item->icon}}"></i></a></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
                <a class="copyright" href="{{'/privacy-policy'}}">{{__("Privacy policy")}}</a>
                <p class="copyright">{{__('&copy; Guangzhou Relycom Trading Co., Ltd., all rights reserved, ')}}2015</p>
            </div>
        </div>
    </div>
</footer>