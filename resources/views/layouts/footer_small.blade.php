<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/18/2018
 * Time: 11:14 AM
 */
?>
<footer>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-5">
                <a class="copyright" href="{{'/privacy-policy'}}">{{__("Privacy policy")}}</a>
                <p class="copyright">{{__('&copy; Guangzhou Relycom Trading Co., Ltd., all rights reserved, ')}}2015</p>
            </div>
        </div>
    </div>
</footer>
