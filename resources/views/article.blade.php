<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/17/2018
 * Time: 5:08 PM
 */
?>
@extends('layouts.layout')

@section('title', __('title.Article'))

@section('meta')
    @parent
    <?php if(!empty($blog)):?>
    <meta name="title" content="{{ $blog->title }}">
    <meta name="description" content="{{ $blog->meta_description }}">
    <meta name="keywords" content="{{ $blog->meta_tag }}">
    <?php endif;?>
@endsection

@section('content')
    @include("components.blogs.view")
@endsection

