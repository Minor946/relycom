<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/18/2018
 * Time: 11:18 AM
 *
 * @var $model \App\Models\PagesBlockModel
 *
 */
?>
<div class="contact-container">
    <section class="section-padding" style="z-index: 1;">
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8 order-pos">
                    @if(isset($model->content))
                        {!! html_entity_decode($model->content) !!}
                    @endif
                </div>
            </div>
            <div class="row contact-block">
                <div class="col-md-2 col-lg-2"></div>
                <div class="col-md-10 col-lg-3" data-aos="fade-up">
                    <a class="navbar-brand" href="#"><img src="{{url('/images/logo_menu.svg')}}" width="120"> </a>
                    <div class="footer-container">
                        <div style="display: flex">
                            <i class="fa fa-map-marker" style="margin: auto 0;">&nbsp;</i>
                            <p style="margin: 0">
                                &nbsp;&nbsp;{!! __("Китай, провинция Гуандун,<br>&nbsp; г. Гуанчжоу, ул Тунхэ 191") !!} </p>
                        </div>
                        <p class="margin-top-5"><i class="fa fa-phone">&nbsp;&nbsp;</i>{{__("+123 456 789 0123")}}</p>
                        <p class="email-link"><a href="mailto:{{__("sales.ru@relycom.net")}}"><i class="fa fa-envelope">&nbsp;&nbsp;</i>{{__("sales.ru@relycom.net")}}  </a> </p>
                    </div>
                </div>
                @tablet
                <div class="col-md-2"></div>
                @endtablet
                <div class="col-md-10 col-lg-3 social-container" data-aos="fade-up">
                    <h4>{{__('Ждем вас в соцсетях')}}</h4>
                    <ul class="social-detail padding-top-5">
                        <li>{{__('Там новости')}}</li>
                        <li>{{__('Консультация')}}</li>
                        <li>{{__('Ответы на вопросы')}}</li>
                    </ul>
                    <ul class="footer-social" data-aos="fade-up">
                        <?php $social = \App\Models\SocialModels::where('status', 1)->get();?>
                        <?php if(!empty($social)):?>
                        <?php foreach ($social as $item):?>
                        <li><a href="{{$item->url}}" target="_blank"><i class="fa fa-2x {{$item->icon}}"></i></a></li>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="section-blue section-form label-white section-padding">
        <div>
            @desktop
            <div id="logo-contact" class="logo-contact-anim"></div>
            @elsedesktop
            <img class="logo-contact" src="{{url("/images/logo_back.svg")}}">
            @enddesktop

        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-6">
                    <h2 data-aos="fade-up">{{__('Напишите нам')}}</h2>

                    {!! Form::open(['action' => 'MainController@callback', 'id'=>'contacts-form','class'=>'checklist-form',
                    'data-aos'=>'fade-up', 'data-parsley-validate'=>""]) !!}
                    <div class="form-group">
                        <input type="text" name="name" class="form-control"
                               data-parsley-trigger="change" required=""
                               data-parsley-required-message="{{__('Mandatory field')}}"
                               placeholder="{{__('Ваше имя')}}">
                    </div>

                    <div class="form-group">
                        <input type="email" name="email" class="form-control"
                               data-parsley-trigger="change" required=""
                               data-parsley-required-message="{{__('Mandatory field')}}"
                               placeholder="{{__('E-mail')}}">
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="service"
                                data-parsley-required-message="{{__('Mandatory field')}}"
                                data-parsley-trigger="change" required="">
                            <option value="" selected
                                    disabled>{{__('Выберите услугу, которая Вас интересует')}}</option>
                            <?php
                            $model = \App\Models\PagesModel::where('type', 0)->where('locale', Lang::getLocale())->orderBy('position', 'ASC')->get();
                            ?>
                            <?php foreach ($model as $item):?>
                            <option value="{{$item->title_menu}}">{{$item->title_menu}}</option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="form-group">
                            <textarea class="form-control" name="msg"
                                      data-parsley-required-message="{{__('Mandatory field')}}"
                                      placeholder="{{__('Сообщение')}}"  data-parsley-trigger="change" required=""
                                      rows="6"></textarea>
                    </div>
                    <div align="right">
                        <button type="submit" class="btn btn-outline-light btn-transparent">{{__('Отправить')}}</button>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </section>
</div>
@section('scripts')
    @parent
    $(document).ready(function ($) {
    animatePage('logo-contact', '{{url('/animate/contacts.json')}}', false);
    $('.navbar').addClass('bg-white');
    });
@endsection

<?php $showFlash = session('showFlash'); ?>
<?php if($showFlash && !empty($showFlash)):?>
<div class="Message Message--green">
    <div class="Message-icon">
        <i class="fa fa-check"></i>
    </div>
    <div class="Message-body">
        <p>{{__('We will contact you shortly')}}</p>
    </div>
    <button class="Message-close js-messageClose"><i class="fa fa-times"></i></button>
</div>
{{ Session::put('showFlash', false)}}
<?php endif;?>

<script>
    $(function () {
        $('#contacts-form').parsley().on('field:validated', function() {
            var ok = $('.parsley-error').length === 0;
            $('.bs-callout-info').toggleClass('hidden', !ok);
            $('.bs-callout-warning').toggleClass('hidden', ok);
        })
            .on('form:submit', function() {
                return false; // Don't submit form for this demo
            });
    });
</script>
