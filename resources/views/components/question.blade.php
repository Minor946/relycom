<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 07.01.2019
 * Time: 1:38
 */
?>

<div class="padding-bottom-70" align="right">
    <a  class="connect-link" href="{{url('/contacts')}}">
        <div class="connect-to-us">
            <div class="question-container">
                <div class="connect-text" align="left">
                    <strong>{{__("Остались вопросы?")}}</strong>
                    <p>{{__("Свяжитесь с нами!")}}</p>
                </div>
            </div>
        </div>
    </a>
</div>
