<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/18/2018
 * Time: 11:56 AM
 */
?>

<section class="section-grey">
    <div class="container section-padding">
        <div class="row">
            <div class="col-12 col-md-5 anim-container" align="right">
                @desktop
                <div id="logo-logistic" class="service-icon-anim logo-logistic-anim"></div>
                @elsedesktop
                <div class="icons-service">
                    <img class="service-icon truck-rotate" src="{{url("/images/truck.svg")}}">
                    <div class="service-icon truck-dot"></div>
                    <img class="service-icon truck-check" src="{{url("/images/tick-inside-circle.svg")}}">
                </div>
                @enddesktop
            </div>
            <div class="col-12 col-md-6" id="logistic-body" data-aos="fade-up">
                <div class="label-section" align="left">
                    <h3><?=$model->title?></h3>
                </div>
                <div data-aos="fade-up" id="service-text">
                    {!! html_entity_decode($model->description) !!}
                </div>
            </div>
        </div>
    </div>
    @include('components.question')
</section>
<section class="section-padding bg-white" data-aos="fade-up">
    @include('components.menu', ['type' => 0])
</section>

@section('scripts')
    @parent
    $(document).ready(function ($) {
    animateLogistic('{{url('/animate/logistic_2.json')}}');
    });
@endsection