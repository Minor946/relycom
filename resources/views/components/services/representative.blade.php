<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/18/2018
 * Time: 11:56 AM
 */
?>

<section class="section-grey">
    <div class="container section-padding">
        <div class="row">
            <div class="col-12 col-md-5 anim-container" align="right">
                @desktop
                <div id="logo-representative" class="service-icon-anim logo-logistic-anim"></div>
                @elsedesktop
                <img class="service-icon" src="{{url("/images/representative-in-china.svg")}}">
                @enddesktop
            </div>
            <div class="col-12 col-md-6">
                <div class="label-section" align="left">
                    <h3><?=$model->title?></h3>
                </div>
                <div data-aos="fade-up" id="service-text">
                    {!! html_entity_decode($model->description) !!}
                </div>
            </div>
        </div>
    </div>
    @include('components.question')
</section>
<section class="section-padding bg-white"  data-aos="fade-up" >
    @include('components.menu', ['type' => 0])
</section>
@section('scripts')
    @parent
    $(document).ready(function ($) {
    animateLogistic('{{url('/animate/representative.json')}}', 'logo-representative');
    });
@endsection