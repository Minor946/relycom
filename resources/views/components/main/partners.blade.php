<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/18/2018
 * Time: 9:55 AM
 */
?>
<section class="section-padding bg-white">
    <div class="container">
        <div data-aos="fade-up" class="label-section" align="center">
            <h2>{{__("main.Partners")}}</h2>
        </div>
        <div class="partners-container">
            <?php $partners = \App\Models\PartnersModels::where('status', \App\Models\PartnersModels::STATUS_ACTIVE)
                ->orderBy('position', 'desc')->get();?>
            @if (!empty($partners))
                @foreach($partners as $partner)
                        <img data-aos="fade-up" src="{{url("/storage/".$partner->image)}}" alt="{{$partner->name}}">
                @endforeach
            @endif
        </div>
    </div>
</section>