<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/17/2018
 * Time: 5:00 PM
 */
?>

<section class="section-grey section-logo">
    <div class="container">
        <div class="row">
            <div class="col-md-5"></div>
            <div class="col-md-6">
                <a id="big-logo" class="navbar-brand margin-top-80" href="#"><img src="{{url('/images/logo_menu.svg')}}" width="450"> </a>
            </div>
        </div>
    </div>
</section>
