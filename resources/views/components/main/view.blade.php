<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/18/2018
 * Time: 10:10 AM
 */

use App\Models\PagesBlockModel;use App\Models\PagesModel;use Illuminate\Support\Facades\Lang;

$main_block = PagesBlockModel::getByLocalAll(PagesBlockModel::TYPE_MAIN, Lang::getLocale());
$services = \App\Models\ServiceMainModel::where('locale', Lang::getLocale())->get();
?>

<section class=" bg-white">
    @mobile
    <div class="main-icon-service">
        <img class="service-icon box-icon" src="{{url("/images/box.svg")}}">
        <div class="service-icon box-dot1"></div>
        <img class="service-icon plane-icon" src="{{url("/images/plane.svg")}}">
        <div class="service-icon box-dot2"></div>
        <img class="service-icon plane-check" src="{{url("/images/tick-inside-circle.svg")}}">
    </div>
    @endmobile
    <div>
        <div class="container padding-top-60">
            <div class="row">
                <div class="col-md-5 anim-container1" align="right">
                    @desktop
                    <div class="anim-box-block">
                        <div id="logo-main-box"></div>
                    </div>
                    @enddesktop
                    @tablet
                    <img class="service-icon service-icon-main box-icon" src="{{url("/images/box.svg")}}">
                    <div class="service-icon service-icon-main  box-dot1"></div>
                    <img class="service-icon service-icon-main  plane-icon" src="{{url("/images/plane.svg")}}">
                    @endtablet
                </div>
                <div class="col-md-6  section-padding">
                    <div data-aos="fade-up" class="label-section" align="left">
                        <h3>{{__("RELYCOM - Ваш надежный партнер в Китае")}}</h3>
                    </div>
                    <ul class="main-list">
                        @if(!empty($main_block))
                            @foreach( $main_block as $block)
                                <li data-aos="fade-up">
                                    {!! html_entity_decode($block->content) !!}
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-blue section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-5 anim-container2">
                    @desktop
                    <div id="logo-main-plane"></div>
                    @enddesktop
                    @tablet
                    <div class="service-icon service-icon-main box-dot2"></div>
                    <img class="service-icon service-icon-main plane-check"
                         src="{{url("/images/tick-inside-circle-yellow.svg")}}">
                    @endtablet
                </div>
                <div class="col-md-6">
                    <div class="label-section label-white" align="left">
                        <h3>{{__("Что мы можем сделать для Вас?")}}</h3>
                    </div>
                    <ul class="main-list label-white">
                        @if(!empty($services))
                            @foreach( $services as $service)
                                <li class='service-item' data-aos-duration="1500" data-aos="fade-up">
                                    <?php $page = PagesModel::find($service->page_id);?>
                                    <a href="{{url('/'.$page->alias)}}">
                                        <div class="row">
                                            <div class="col-auto">
                                                <img class="main-icon"
                                                     src="{{url("/storage".$service->image)}}"
                                                     alt="search">
                                            </div>
                                            <div class="col">
                                                <h5>{{$service->title}}</h5>
                                                <p>
                                                    {!! html_entity_decode($service->description) !!}
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="label-section label-white padding-top-60" align="center" data-aos-duration="1500"
             data-aos="fade-up">
            <h3>{{__('Восток – дело тонкое, но нам известны все его тонкости …')}}</h3>
        </div>
    </div>
</section>

@section('scripts')
    @parent
    $(document).ready(function ($) {
    animateLogistic('{{url('/animate/home_box.json')}}','logo-main-box');
    var first = true;
    $(window).scroll(function () {
        if($('#logo-main-plane').visible() && first){
            first = false;
            animateLogistic('{{url('/animate/home_plane.json')}}','logo-main-plane');
        }
    });

    });
@endsection

