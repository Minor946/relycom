<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 12/18/2018
 * Time: 10:52 AM
 *
 * @var $model \App\Models\PagesBlockModel
 */
?>

@if(!empty($models))
    @foreach( $models as $key => $model )
        @if( $key == 0)
            <section class="section-grey section-logo">
                <div class="container">
                    <div class="row justify-content-md-center">
                        <div class="col-md-9" @desktop data-aos="fade-up" data-aos-anchor-placement="top-center"
                             @enddesktop>
                            <div class="first-about-block">
                                {!! html_entity_decode($model->content) !!}
                            </div>
                        </div>
                    </div>
                </div>
                @desktop
                <div class="about-logo-animate">
                    <div id="logo-about" class="logo-about-anim"></div>
                </div>
                @elsedesktop
                <img class="logo-about" src="{{url("/images/logo_animation.svg")}}">
                @enddesktop
            </section>
        @else
            <section class="@if ($key % 2 == 1) section-white @else section-blue label-white @endif section-padding">
                <div class="container">
                    <div class="row justify-content-md-center">
                        <div class="col-md-9" @desktop data-aos="fade-up" data-aos-anchor-placement="center-bottom" @enddesktop>
                            {!! html_entity_decode($model->content) !!}
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endforeach
@endif
@section('scripts')
    @parent
    $(document).ready(function ($) {
        animatePage('logo-about', '{{url('/animate/about.json')}}', false);
    });
@endsection