<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 26.12.2018
 * Time: 12:41
 */
?>
@if($type == 0)
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-5" align="right">
            </div>
            <div class="col-12 col-md-6">
                <ul class="menu">
                    <?php
                    $model = \App\Models\PagesModel::where('type', 0)->where('locale', Lang::getLocale())->orderBy('position', 'ASC')->get();
                    ?>
                    <?php foreach ($model as $key => $item):?>
                    @if (str_replace(\Illuminate\Support\Facades\Lang::getLocale().'/','',Request::path()) != $item->alias)
                        <li>
                            <a class="<?= ($key == 0) ? 'active' : ''?>"
                               href="{{url('/'.$item->alias)}}">{{$item->title_menu}}</a>
                        </li>
                    @endif
                    <?php endforeach;?>

                </ul>
            </div>
        </div>
    </div>
@elseif($type == 1)
    <?php
    $models = \App\Models\CategoryModel::where('status', 1)->where('local', \Illuminate\Support\Facades\Lang::getLocale())->get();
    ?>
    <?php if(!empty($models)):?>
    <ul class="menu menu-case">
        <?php foreach ($models as $key => $item):?>
        <li>
            <a href="{{url('/articles/'.$item->alias)}}">{{$item->name}}</a>
        </li>
        <?php endforeach;?>
    </ul>

    <?php endif;?>
@elseif($type == 2)
    <?php
            if(!empty($article))
    $models = \App\Models\BlogsModels::where('status', 1)->whereRaw('id != ' . $article)->where('locale', \Illuminate\Support\Facades\Lang::getLocale())
        ->get();
            else
                $models = \App\Models\BlogsModels::where('status', 1)->where('locale', \Illuminate\Support\Facades\Lang::getLocale())
                    ->get();
    ?>
    <?php if(!empty($models)):?>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-5" align="right">
            </div>
            <div class="col-12 col-md-6">
                <strong class="menu-label">{{__('menu.similar')}}</strong>
                <ul class="menu menu-blog">
                    <?php foreach ($models as $key => $item):?>
                    <li>
                        <a @if($key == 0) class='active'
                           @endif href="{{url("/article/".$item->alias)}}"><?=$item->title?></a>
                    </li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
    </div>
    <?php endif;?>
@endif
