<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 26.12.2018
 * Time: 23:54
 */
use Fomvasss\Youtube\Facades\Youtube;?>

@mobile
<section class="section-padding">
    <form class="search-container">
        <input type="text" id="search-bar" placeholder="">
        <a href="#"><img class="search-icon" src="{{url('/images/magnifier.svg')}}"></a>
    </form>
    @include('components.menu', ['type' => 1])
</section>
@endmobile
<section class="section-padding @mobile bg-white @endmobile">
    <div>
        <div class="container section-padding">
            <div class="row">
                <div class="col-12 col-md-1"></div>
                <div class="col-12 col-md-4">
                    @notmobile
                    <div class="row stick-search">
                        <div class="col-12 col-lg-2"></div>
                        <div class="col-12 col-md-10 col-lg-10">
                            <div class="margin-left-20">
                                {!! Form::open(['action' => 'MainController@search','method' => 'get',
                                 'id'=>'search-form','class'=>'search-container']) !!}
                                <input type="text" name="search" id="search-bar" placeholder="
                                            @if(!empty($_GET['search'])) <?=$_GET['search']?> @endif">
                                <a href="#"><img class="search-icon" src="{{url('/images/magnifier.svg')}}"></a>
                                {!! Form::close() !!}
                                @include('components.menu', ['type' => 1])
                            </div>
                        </div>
                    </div>
                    @endnotmobile
                </div>
                <div class="col-12 col-md-7 col-lg-7 article-list">
                    <?php if(!empty($error)):?>
                    <h4>{{__('articles.Not available in your language') }} </h4>
                    <?php else:?>
                    <div class="blog-description">
                        <h2>{{$blog->title}}</h2>
                        <?php if(!empty($blog->video)):?>
                        <?= Youtube::iFrame($blog->video)?>
                    </div>
                    <?php else: ?>
                    <img src="{{url('/storage/'.$blog->image)}}">
                    <?php endif;?>
                    <p>
                        {!! html_entity_decode($blog->body) !!}
                    </p>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<section class="section-padding bg-white" data-aos="fade-up">
    @include('components.menu', ['type' => 2, 'article'=>(!empty($blog->id)) ? $blog->id : ''])
</section>