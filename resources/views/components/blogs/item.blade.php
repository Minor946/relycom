<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 26.12.2018
 * Time: 23:54
 *
 * @var $blog \App\Models\PagesModel
 *
 */
?>

<div class="blog-item">
    <div>
        <img src="{{url('/storage/'.$blog->image)}}" width="230">
    </div>
    <div class="blog-desc">
        <h2>{{$blog->title}}</h2>
        <p>
            {!! html_entity_decode($blog->short_desc) !!}
        </p>
    </div>
</div>
