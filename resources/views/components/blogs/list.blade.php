<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 26.12.2018
 * Time: 23:54
 */
?>

<section class="section-padding">
    <div>
        <div class="container section-padding">
            <div class="row">
                <div class="col-12 col-md-1"></div>
                <div class="col-12 col-md-4">
                    <div class="row stick-search">
                        <div class="col-12 col-lg-2"></div>
                        <div class="col-12 col-md-10 col-lg-10">
                            <div class="margin-left-20">
                                {!! Form::open(['action' => 'MainController@search','method' => 'get',
                                  'id'=>'search-form','class'=>'search-container']) !!}
                                <input type="text" name="search" id="search-bar" placeholder="
                                    @if(!empty($_GET['search'])) <?=$_GET['search']?> @endif">
                                <a href="#"><img class="search-icon" src="{{url('/images/magnifier.svg')}}"></a>
                                {!! Form::close() !!}
                                @include('components.menu', ['type' => 1])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-7 col-lg-7 article-list">
                    @if(!empty($blogs) && count($blogs) > 0)
                        @foreach($blogs as $blog)
                            <a class="blog-link" href="{{url('/article/'.$blog->alias)}}">
                                @include ('components.blogs.item', ['blog'=>$blog])
                            </a>
                        @endforeach
                    @else
                        <h4>{{__('articles.emptyArticles') }} </h4>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    body {
        overflow-x: hidden !important;
    }
</style>