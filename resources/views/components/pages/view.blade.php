<?php
/**
 * Created by PhpStorm.
 * User: Minor946
 * Date: 07.01.2019
 * Time: 16:35
 */
?>
<section class="section-grey">
    <div class="container section-padding">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="label-section" align="left">
                    <h3><?=$model->title?></h3>
                </div>
                <div >
                    {!! html_entity_decode($model->description) !!}
                </div>
            </div>
        </div>
    </div>
</section>