<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {
        Route::get('/', 'MainController@index');

        Route::get('/articles', 'MainController@articles');
        Route::get('/articles/{alias}', 'MainController@articles');

        Route::get('/article/{alias}', 'MainController@article');

        Route::get('/contacts', 'MainController@contacts');
        Route::get('/about', 'MainController@about');

        Route::post('/callback', 'MainController@callback');
        Route::get('/search', 'MainController@search');

        Route::get('/{alias}', 'MainController@pages');
    }
);