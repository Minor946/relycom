$(document).ready(function () {

    $(".hamburger").on('click', function () {
        $(this).toggleClass('is-active');
        $('#first-logo').toggle('slow');
    });
});

function animatePage(element, data, loop = true, speed = 1) {
    var animData = {
        wrapper: document.getElementById(element),
        animType: 'svg',
        loop: loop,
        prerender: true,
        autoplay: true,
        path: data
    };
    var anim = bodymovin.loadAnimation(animData);

    if(element === 'logo-about'){
        anim.setSpeed(1.2);
    }

    if(element ==='loader-brand'){
        anim.addEventListener('DOMLoaded', function (e) {
            anim.setSpeed(0.8);
            anim.playSegments([0, 50], true); // run intro animation
        });
        anim.addEventListener('complete', doalert);
        function doalert() {
            $.when($('#loader').fadeOut(600))
                .done(function() {
                    $('body').addClass('loaded');
                    $('body').css("overflow", "auto");
                    $('.wrapper').css("margin-top", "70px");
            });
        }
    }else{
        anim.play();
    }
};

function animateLogistic(url, id = 'logo-logistic', speed = 1200) {
    var animDataLogistic = {
        wrapper: document.getElementById(id),
        animType: 'svg',
        loop: false,
        prerender: true,
        autoplay: false,
        path: url
    };
    var anim = bodymovin.loadAnimation(animDataLogistic);

    $(window).scroll(function () {
        var w = $(window).scrollTop();
        if (id === 'logo-main-box') {
            var el = $('.anim-container1');
        } else if (id === 'logo-main-plane') {
            var el = $('.anim-container2');
            if (w < 1050) {
                return;
            }
            w = w - 1050;
        } else {
            var el = $('.anim-container');
        }
        switch (id) {
            case 'logo-logistic':
                rate = 7;
                break;
            case 'logo-quality':
                rate = 28;
                break;
            case 'logo-representative':
                rate = 42;
                break;
            case 'logo-analysis':
            case 'logo-search':
                rate = 10;
                break;
            case 'logo-translator':
                rate = 25;
                break;
            case 'logo-main-plane':
                rate = 8;
                break;
            default:
                rate = 10;
                break;
        }

        var scrollPercent = (100 * Math.abs(w)) / (el.height() - 100);
        var totalDuration = el.height() / anim.totalFrames;
        var total = anim.totalFrames / (Math.round(totalDuration * 100) / 100) / 10;

        total = Math.round(total * 100) / 100;
        if (total <= 1) {
            total = 1;
        }
        var scrollPercentRounded = Math.round(scrollPercent * rate);
        if (scrollPercentRounded === 0) {
            anim.goToAndStop(0)
        }
        anim.goToAndStop(scrollPercentRounded * total)

    });
};


function animateMain(url, el) {
    var animDataMain = {
        wrapper: document.getElementById(el),
        animType: 'svg',
        loop: false,
        prerender: true,
        autoplay: false,
        path: url
    };
    var anim = bodymovin.loadAnimation(animDataMain);
    $(window).scroll(function () {
        var scrollPercent = 100 * $(window).scrollTop() / ($(document).height() - $(window).height());
        var totalDuration = anim.totalFrames / anim.frameRate * 1000;

        scrollPercentRounded = Math.round(scrollPercent);
        if (scrollPercentRounded === 0) {
            anim.goToAndStop(0)
        }
        anim.goToAndStop((((scrollPercentRounded) / 100) * 1100))
    });
};

$(document).ready(function ($) {
    AOS.init();


    $('.menu a').hover(function () {
        $('.menu a').removeClass('active')
    });
});

showCookie();

function showCookie() {
    if (Cookies.get('show_cookie') !== 'true') {
        setTimeout(function () {
            $("#cookieConsent").fadeIn(200);
        }, 4000);
        $("#closeCookieConsent, .cookieConsentOK").click(function () {
            $("#cookieConsent").fadeOut(200);
            Cookies.set('show_cookie', 'true', {expires: 7});
        });
    }

}

$('.js-messageClose').on('click', function(e) {
    closeMessage($(this).closest('.Message'));
});

function closeMessage(el) {
    el.addClass('is-hidden');
}