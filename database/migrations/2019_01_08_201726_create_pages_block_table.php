<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesBlockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages_block', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type');
            $table->mediumText('content');
            $table->string('local');
            $table->integer('position');
            $table->integer('status');
            $table->integer('background')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages_block');
    }
}
